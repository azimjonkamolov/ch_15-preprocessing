// C15_37.c
#include<stdio.h>

#define LEVEL 1

int main()
{
	
#ifdef LEVEL
	printf("Expert!\n");
#else
	printf("Beginner!\n");
#endif

	return 0;
}
