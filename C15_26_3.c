// C15_26.c
#include<stdio.h>

#define ODD(x) (x%2!=0)

int main()
{
	int n;
	
	scanf("%d", &n);
	printf("%d", ODD(n));
	
	return 0;
}
