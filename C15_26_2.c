// C15_26_2.c
#include<stdio.h>

#define FIVE(X) (X*5)

int main()
{
	int a;
	printf("Enter the number:");
	scanf("%d", &a);
	printf("The answer: %d\n", FIVE(a));
	
	return 0;	
} 
