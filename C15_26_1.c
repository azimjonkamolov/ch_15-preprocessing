// C15_26_1.c
#include<stdio.h>

#define MIN(x,y) ((x) <= (y)) ? (x) : (y)

int main()
{
	int a,b;
	printf("Enter the numbers:");
	scanf("%d %d", &a, &b);
	printf("Min: %d\n", MIN(a,b));
	
	return 0;	
} 
