// C15_33.2.c
#include<stdio.h>

#define TEST 1
void myfunc();

#if TEST
void main()
{
	myfunc();
}
#endif

void myfunc()
{
	printf("Hey there!\n");
}
