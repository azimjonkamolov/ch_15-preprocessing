// C15_48.c
#include<stdio.h>
#include"myfunc1.c"
#include"myfunc2.c"
int myfunc1(int x, int y);
void myfunc2(int n);

int main()
{
	int a, b, sum=0;
	scanf("%d %d", &a, &b);
	sum=myfunc1(a,b);
	myfunc2(sum);
	
	return 0;
}
