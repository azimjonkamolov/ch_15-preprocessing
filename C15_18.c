// C15_18.c
#include<stdio.h>

#define PI 3.1415

int main()
{
	int r;
	double cir;
	double area;
	
//	r=2;
//	cir=2*3.14*r
//	area=3.14*r*r; wanna change 3.14 to 3.1415 then an easy way

	r=2;
	cir=2*PI*r;
	area=PI*r*r;
	
	printf("cir = %.2f, area=%.2f\n", cir, area);
	return 0;
}
