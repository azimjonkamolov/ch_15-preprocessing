// C15_24.c
#include<stdio.h>
#define PI 3.14

#define SQUARE(x) (x*x)

int main()
{
	double area;
	area=PI*SQUARE(4);	// area = 3.14*(4*4)
	
	printf("area = %f\n", area);
	
	return 0;
}
